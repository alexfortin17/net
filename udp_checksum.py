"""
12:59:44.195470 IP (tos 0x80, ttl 3, id 19342, offset 0, flags [DF], proto UDP (17), length 47)
10.33.14.1.55422 > 10.32.64.36.20000: [bad udp cksum 0x6ae9 -> 0x7cd7!] UDP, length 19
0x0000:  ffff ffff ffff ea4a 16d6 61d5 0800 4580
0x0010:  002f 4b8e 4000 0311 c94a 0a21 0e01 0a20
0x0020:  4024 d87e 4e20 001b 6ae9 18e0 403e 0a21                   #6ae9 is WRONG
0x0030:  1001 0000 0000 0a21 0e14 6466 0a
src ip : 0a21 0e01 (32)
dest ip: 0a20 4024 (32)
src port: d87e (16)
dst port: 4e20 (16)
lenght: 001b (16)
Protocol type: 0011 (16)                            
Data: 18e0 403e 0a21 1001 0000 0000 0a21 0e14 6466 0a (152)
"""

"""
Binary addition with wraparounf on max size (in bits)
Ex: for 8 bits: 255(11111111) + 2 (00000010) = 2
"""
def binary_addition(num1,num2,bits):
        MOD = 1 << bits
        result = num1 + num2
        return result if result < MOD else (result+1) % MOD

"""
Positive Binary complement 
Ex: 0101(5) -> 1010(10) (NOT fuckin -2 like its a signed int)
"""
def bswitch(num):
    c = 1         
    while num*2 > c:
        num = num ^ c
        c = c << 1
    return num 

def main():
    #Pseudo Header Info
    #src_ip = 0x0A210E01
    src_ip_word_0 = 0x0A21 
    src_ip_word_1 = 0x0E01
    #dst_ip = 0x0A204024
    dst_ip_word_0 = 0x0A20 
    dst_ip_word_1 = 0x4024 
    proto = 0x0011 
    pseudo_length = 0x001B # THIS IS THE SAME THING THAT IS ALSO IN THE UDP HEADER 
    pseudo_header_word_list = [src_ip_word_0, src_ip_word_1,
            dst_ip_word_0, dst_ip_word_1, proto, pseudo_length]

    #UDP Header info
    src_port = 0xD87E
    dst_port = 0x4E20
    length = 0x001B # THIS IS THE SAME THING THAT IS IN THE PSEUDO HEADER
    udp_header_word_list = [src_port, dst_port, length]

    #Payload info
    data_0 = 0x18e0
    data_1 = 0x403e
    data_2 = 0x0A21 
    data_3 = 0x1001
    data_4 = 0x0000
    data_5 = 0x0000
    data_6 = 0x0A21 
    data_7 = 0x0e14
    data_8 = 0x6466
    data_9 = 0x0A00 #some padding here
    data_word_list = [data_0, data_1, data_2, data_3, data_4,
            data_5, data_6, data_7, data_8, data_9]

    inter = 0 
    # For our 3 lists, we do a 16 bit binary addition 
    for sublist in [pseudo_header_word_list, udp_header_word_list, data_word_list]:
        for x in sublist:
            inter = binary_addition(inter, x, 16)

    check = bswitch(inter)

    print("The UDP Checksum is: {}".format(hex(check)))

if __name__ == "__main__":
    main()
